using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [SerializeField] private NavMeshAgent agent;
    [SerializeField] private Transform weaponTransform;
    [SerializeField] private Weapon weapon;

    void Start()
    {
        Debug.Log(agent.Warp(transform.position));
    }

    void Update()
    {
        Player player = Player.Self;
        if (player != null)
        {
            transform.rotation =
                Quaternion.LookRotation((player.transform.position - transform.position).normalized, Vector3.up);
            
            agent.acceleration = SpeedController.Self.Speed * 3.5f;
            agent.speed = SpeedController.Self.Speed * 14f;
            agent.SetDestination(player.transform.position);
        }
        
        weapon.Shoot();
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : Singleton<EnemySpawner>
{
    [SerializeField] private List<Transform> spawnTransforms = new List<Transform>();
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private float spawnDelay = 2.5f;

    private List<GameObject> enemyObjects = new List<GameObject>();
    private bool active = true;

    [SerializeField] private AudioSource audioSource;

    protected override void Awake()
    {
        base.Awake();
        self = this;
    }

    private void Start()
    {
        StartCoroutine(SpawnRoutine());
    }

    private void SpawnEnemy()
    {
        if (active)
        {
            GameObject enemyObject = null;
            while (enemyObject == null)
            {
                int random = Random.Range(0, spawnTransforms.Count);
                if (Vector3.Distance(Player.self.transform.position, spawnTransforms[random].position) > 35f)
                {
                    enemyObject = Instantiate(enemyPrefab, spawnTransforms[random].position, Quaternion.identity);
                    enemyObjects.Add(enemyObject);
                }
            }
        }
        
    }

    private IEnumerator SpawnRoutine()
    {
        while (this != null)
        {
            yield return new WaitForSeconds(spawnDelay / SpeedController.self.Speed);
            SpawnEnemy();
        }
    }

    public void RemoveEnemiesAndStop()
    {
        foreach (var enemyObject in enemyObjects)
        {
            if (enemyObject != null)
            {
                Destroy(enemyObject);
            }
        }

        active = false;
    }

    public void Restart()
    {
        active = true;
    }
}

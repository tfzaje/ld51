﻿namespace DefaultNamespace.Enums
{
    public enum EWeaponType
    {
        Gun,
        RocketLauncher,
        GrenadeLauncher,
        FlameThrower
    }
}

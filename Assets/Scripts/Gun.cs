using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class Gun : Weapon
{
    [SerializeField] private bool isPlayerGun = false;
    [SerializeField] private AudioSource audioSource;

    public override Projectile Shoot()
    {
        if (isPlayerGun)
        {
            if (ammo <= 0)
            {
                Player.Self.RemoveCurrentWeapon();
                return null;
            }
        }
        if (delayActive)
            return null;

        if (isPlayerGun)
        {
            ammo--;
            GunUI.Self.UpdateAmmoText(ammo, maxAmmo);
        }
        
        audioSource.Play();
        
        GameObject projectileObject = Instantiate(projectilePrefab, spawnTransform.position, Quaternion.identity);
        projectileObject.transform.forward = spawnTransform.forward;

        StartCoroutine(DelayRoutine());

        return projectileObject.GetComponent<Projectile>();
    }
}

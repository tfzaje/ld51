using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace.UI;
using UnityEngine;

public class HealthManager : MonoBehaviour
{
    [SerializeField] private int maxHealth = 1;
    [SerializeField] private int health = 1;
    [SerializeField] private bool isEnemy = true;

    private event EventHandler eventHandler;
    
    void Start()
    {
        health = maxHealth;
        if (isEnemy)
        {
            eventHandler += Death;
        }

        if (!isEnemy)
        {
            eventHandler += PlayerDeath;
            HealthUI.Self.UpdateHealth(health);
        }
        
    }

    private void Death(object sender, EventArgs e)
    {
        HighScoreUI.Self.AddScore(100);
        eventHandler -= Death;
        Destroy(gameObject);
    }

    private void PlayerDeath(object sender, EventArgs e)
    {
        EnemySpawner.Self.RemoveEnemiesAndStop();
        Player.Self.Deactivate();
        HighScoreUI.Self.ShowButton();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }

    public void ApplyDamage(int damage)
    {
        health -= damage;

        if (!isEnemy)
            HealthUI.Self.UpdateHealth(health);
        
        if (health <= 0)
        {
            eventHandler?.Invoke(this, null);
        }
    }

    public void Restart()
    {
        health = maxHealth;
        if (!isEnemy)
            HealthUI.Self.UpdateHealth(health);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox : MonoBehaviour
{
    [SerializeField] private HealthManager healthManager;

    public HealthManager HealthManager => healthManager;
}

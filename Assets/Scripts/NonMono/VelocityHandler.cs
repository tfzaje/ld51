﻿using UnityEngine;
using Vector3 = UnityEngine.Vector3;

namespace DefaultNamespace.NonMono
{
    public class VelocityHandler : MonoBehaviour
    {
        private Vector3 velocityDir = Vector3.zero;
        private float velocity = 0f;
        private Vector3 gravityVelocity = Vector3.zero;
        private float maxVelocity = 15f;
        private float gravity = 30f;
        private float acceleration = 4f;
        

        public void Stop()
        {
            if (Player.Self.IsGrounded)
            {
                velocity = 0;
            }
        }

        public void UpdateGravity(CharacterController characterController)
        {
            if (Player.Self.IsGrounded)
            {
                gravityVelocity = Vector3.zero;
            }
            else
            {
                gravityVelocity += Vector3.down * gravity * Time.deltaTime;
                characterController.Move(gravityVelocity * Time.deltaTime * SpeedController.Self.Speed);
            }
        }

        public void AddVelocity(Vector3 dir)
        {
            velocityDir = dir;
            
            if (Player.Self.IsGrounded)
            {
                velocity += acceleration * Time.deltaTime;
            }
            else
            {
                velocity += acceleration * Time.deltaTime * 0.1f;
            }

            if (velocity > maxVelocity * SpeedController.Self.Speed)
            {
                velocity = maxVelocity * SpeedController.Self.Speed;
            }
        }

        public void Jump(CharacterController characterController)
        {
            gravityVelocity.y += gravity * 0.5f;
            characterController.Move(gravityVelocity * Time.deltaTime * SpeedController.Self.Speed);
        }

        public Vector3 VelocityDir => velocityDir;

        public float Velocity => velocity;

        public Vector3 GravityVelocity => gravityVelocity;

        public float MaxVelocity => maxVelocity;

        public float Gravity => gravity;

        public float Acceleration => acceleration;
    }
    
}

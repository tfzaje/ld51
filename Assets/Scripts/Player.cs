using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using DefaultNamespace.Enums;
using DefaultNamespace.NonMono;
using UnityEngine;

public class Player : Singleton<Player>
{
    [SerializeField] private HealthManager healthManager;
    [SerializeField] private CharacterController characterController;
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private LayerMask nonPlayerLayer;

    [SerializeField] private List<Weapon> weapons = new List<Weapon>();
    private Dictionary<EWeaponType, Weapon> weaponDict = new Dictionary<EWeaponType, Weapon>();

    private Weapon currentWeapon;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private VelocityHandler velocityHandler;
    private bool isGrounded = false;

    private bool active = true;

    public void CheckGrounded()
    {
        isGrounded = Physics.CheckSphere(transform.position, 0.2f, nonPlayerLayer);
    }

    protected override void Awake()
    {
        base.Awake();
        self = this;
    }

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        foreach (var weapon in weapons)
        {
            weaponDict.Add(weapon.WeaponType, weapon);
        }
    }

    void Update()
    {
        if (active)
        {
            Controls();
            CheckGrounded();
            velocityHandler.UpdateGravity(characterController);
        }
    }


    private void Controls()
    {
        if (Input.GetMouseButton(0))
        {
            if (currentWeapon != null)
            {
                currentWeapon.Shoot();
            }
        }
        
        bool hasMoved = false;
        Vector3 movementVector = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
        {
            movementVector += transform.forward;
            hasMoved = true;
        }

        if (Input.GetKey(KeyCode.A))
        {
            movementVector += -transform.right;
            hasMoved = true;
        }

        if (Input.GetKey(KeyCode.S))
        {
            movementVector += -transform.forward;
            hasMoved = true;
        }

        if (Input.GetKey(KeyCode.D))
        {
            movementVector += transform.right;
            hasMoved = true;
        }

        if (!hasMoved)
        {
            velocityHandler.Stop();
        }
        else
        {
            velocityHandler.AddVelocity(movementVector.normalized);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isGrounded)
            {
                velocityHandler.Jump(characterController);
            }
        }

        characterController.Move(Time.deltaTime * velocityHandler.VelocityDir * velocityHandler.Velocity * SpeedController.self.Speed);

        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        transform.RotateAround(transform.position, Vector3.up, mouseX * 200f * Time.deltaTime);


        float angleDeltaY = -mouseY * 120 * Time.deltaTime;
        float cameraAngle = -Vector3.SignedAngle(transform.forward, cameraTransform.forward, transform.right);

        if (cameraAngle < -60f)
        {
            if (angleDeltaY < 0f)
            {
                cameraTransform.RotateAround(cameraTransform.position, cameraTransform.right, angleDeltaY);
            }
        }
        else if (cameraAngle > 60f)
        {
            if (angleDeltaY > 0f)
            {
                cameraTransform.RotateAround(cameraTransform.position, cameraTransform.right, angleDeltaY);
            }
        }
        else
        {
            cameraTransform.RotateAround(cameraTransform.position, cameraTransform.right, angleDeltaY);
        }
    }

    public void Pickup(EWeaponType weaponType)
    {
        RemoveCurrentWeapon();
        
        Weapon weapon = weaponDict[weaponType];
        currentWeapon = weapon;
        weapon.ResetAmmo();
        weapon.Activate();
    }

    public void RemoveCurrentWeapon()
    {
        if (currentWeapon != null)
            currentWeapon.Deactivate();
    }

    private void OnTriggerEnter(Collider other)
    {
        WeaponPickup weaponPickup = other.GetComponent<WeaponPickup>();

        if (weaponPickup != null)
        {
            if (weaponPickup.IsActive)
            {
                Pickup(weaponPickup.WeaponType);
                StartCoroutine(weaponPickup.DelayRoutine());
            }
        }
    }

    public void Restart()
    {
        SpeedController.self.Restart();
        HighScoreUI.self.Restart();
        EnemySpawner.self.Restart();
        healthManager.Restart();
        active = true;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void Deactivate()
    {
        active = false;
    }

    public bool IsGrounded => isGrounded;
}

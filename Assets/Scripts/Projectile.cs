using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private int damage = 1;

    private void Start()
    {
        Destroy(gameObject, 30f);
    }

    void Update()
    {
        transform.Translate(transform.forward * Time.deltaTime * speed * SpeedController.Self.Speed, Space.World);
    }

    private void OnTriggerEnter(Collider other)
    {
        Hitbox hitbox = other.GetComponent<Hitbox>();

        if (hitbox != null)
        {
            hitbox.HealthManager.ApplyDamage(damage);
        }
        
        Destroy(gameObject);
    }
}

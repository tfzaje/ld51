﻿using System;
using UnityEngine;

namespace DefaultNamespace
{
    public class Singleton<T> : MonoBehaviour
    {
        protected static T self;

        protected virtual void Awake()
        {
            if (self != null && !self.Equals(this))
            {
                Destroy(this);
            }
        }

        public static T Self => self;
    }
}
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class SpeedController : Singleton<SpeedController>
{
    private float speed = 1f;
    [SerializeField] private float speedIncrease = 0.1f;
    private int counter = 10;
    
    protected override void Awake()
    {
        base.Awake();
        self = this;
    }

    void Start()
    {
        SpeedUI.Self.UpdateData(speed);
        StartCoroutine(IncreaseSpeedRoutine());
    }

    void Update()
    {
        
    }

    private IEnumerator IncreaseSpeedRoutine()
    {
        while (this != null)
        {
            yield return new WaitForSeconds(1f);
            counter--;
            if (counter == 0)
            {
                counter = 10;
                speed += speedIncrease;
                HighScoreUI.self.AddScore((int)(speed * Time.time * Mathf.PI));
            }
            SpeedUI.Self.UpdateData(speed);
        }
    }

    public void Restart()
    {
        speed = 1;
    }

    public float SpeedIncrease => speedIncrease;

    public float Speed => speed;

    public int Counter => counter;
}

using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using TMPro;
using UnityEngine;

public class GunUI : Singleton<GunUI>
{
    [SerializeField] private TextMeshProUGUI ammoText;
    
    
    protected override void Awake()
    {
        base.Awake();
        self = this;
    }

    public void UpdateAmmoText(int ammo, int maxAmmo)
    {
        ammoText.text = ammo + "/" + maxAmmo;
    }
}

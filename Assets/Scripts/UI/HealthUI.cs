﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace.UI
{
    public class HealthUI : Singleton<HealthUI>
    {
        [SerializeField] private List<Image> healthImages = new List<Image>();
        [SerializeField] private Color activeBarColor;
        [SerializeField] private Color nonActiveBarColor;

        protected override void Awake()
        {
            base.Awake();
            self = this;
        }

        public void UpdateHealth(int health)
        {
            if (health < 0)
                return;
            
            foreach (var healthImage in healthImages)
            {
                healthImage.color = nonActiveBarColor;
            }

            for (int i = 0; i < health; i++)
            {
                healthImages[i].color = activeBarColor;
            }
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class HighScoreUI : DefaultNamespace.Singleton<HighScoreUI>
{
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private GameObject buttonObject;
    private int score = 0;

    private bool active = true;

    protected override void Awake()
    {
        base.Awake();
        self = this;
    }

    public void AddScore(int score)
    {
        if (active)
        {
            this.score += score;
            text.text = this.score.ToString();
        }
    }

    public void Restart()
    {
        active = true;
        
        score = 0;
        AddScore(0);
    }

    public void ShowButton()
    {
        active = false;
        buttonObject.SetActive(true);
    }

    public void RestartPlayer()
    {
        buttonObject.SetActive(false);
        Player.self.Restart();
    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }
}

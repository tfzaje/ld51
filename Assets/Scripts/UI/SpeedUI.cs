using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SpeedUI : Singleton<SpeedUI>
{
    [SerializeField] private TextMeshProUGUI speedMultiplierText;
    [SerializeField] private List<Image> speedBarImages = new List<Image>();
    [SerializeField] private Color activeBarColor;
    [SerializeField] private Color nonActiveBarColor;
    private string prefix { get; } = "Speed: x";
    
    protected override void Awake()
    {
        base.Awake();
        self = this;
    }

    public void UpdateData(float speed)
    {
        UpdateMultiplierText(speed);
        UpdateSpeedBarImages();
    }

    private void UpdateMultiplierText(float speed)
    {
        speed *= 100;
        int temp = (int)speed;
        speed = temp / 100f;

        speedMultiplierText.text = prefix + speed;
    }

    private void UpdateSpeedBarImages()
    {
        foreach (var speedBarImage in speedBarImages)
        {
            speedBarImage.color = nonActiveBarColor;
        }

        for (int i = 0; i < SpeedController.self.Counter; i++)
        {
            speedBarImages[i].color = activeBarColor;
        } 
    }
}

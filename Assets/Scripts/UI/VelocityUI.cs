using System.Collections;
using System.Collections.Generic;
using DefaultNamespace.NonMono;
using UnityEngine;
using UnityEngine.UI;

public class VelocityUI : MonoBehaviour
{
    [SerializeField] private VelocityHandler velocityHandler;
    [SerializeField] private List<Image> velocityImages = new List<Image>();
    [SerializeField] private Color activeVelocityImageColor;
    [SerializeField] private Color allActiveVelocityImageColor;
    [SerializeField] private Color nonActiveVelocityImageColor;

    void Start()
    {
        StartCoroutine(UpdateVelocityBarRoutine());
    }

    void Update()
    {
        
    }

    private void UpdateVelocityBar()
    {
        float maxVelocity = velocityHandler.MaxVelocity - 1f;
        float velocityForOneArrow = maxVelocity / 6f;
        int arrowNumber = (int) (velocityHandler.Velocity / velocityForOneArrow);

        foreach (var velocityImage in velocityImages)
        {
            velocityImage.color = nonActiveVelocityImageColor;
        }

        if (arrowNumber > 6)
            arrowNumber = 6;

        if (arrowNumber < 6)
        {
            for (int i = 0; i < arrowNumber; i++)
            {
                velocityImages[i].color = activeVelocityImageColor;
            }
        }
        else
        {
            for (int i = 0; i < arrowNumber; i++)
            {
                velocityImages[i].color = allActiveVelocityImageColor;
            }
        }
    }

    IEnumerator UpdateVelocityBarRoutine()
    {
        while (this != null)
        {
            yield return new WaitForSeconds(0.1f);
            UpdateVelocityBar();
        }
    }
}

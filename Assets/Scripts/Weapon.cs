﻿using System;
using System.Collections;
using DefaultNamespace.Enums;
using UnityEngine;

namespace DefaultNamespace
{
    public class Weapon : MonoBehaviour
    {
        [SerializeField] private EWeaponType weaponType;
        
        [SerializeField] protected GameObject projectilePrefab;
        [SerializeField] protected Transform spawnTransform;
        [SerializeField] protected Renderer renderer;
        [SerializeField] protected bool active = false;

        [SerializeField] protected float delay = 1f;
        protected bool delayActive = false;
        [SerializeField] protected int maxAmmo = 100;
        [SerializeField] protected int ammo;

        protected virtual void Start()
        {
            ammo = maxAmmo;
            if (!active)
                Deactivate();
        }

        public virtual Projectile Shoot()
        {
            return null;
        }
        
        public void Activate()
        {
            renderer.enabled = true;
            active = true;
            GunUI.Self.UpdateAmmoText(ammo, maxAmmo);
        }

        public void Deactivate()
        {
            renderer.enabled = false;
            active = false;
        }

        public void ResetAmmo()
        {
            ammo = maxAmmo;
        }

        public IEnumerator DelayRoutine()
        {
            delayActive = true;
            yield return new WaitForSeconds(delay / SpeedController.Self.Speed);
            delayActive = false;
        }

        public EWeaponType WeaponType => weaponType;
    }
}
﻿using System.Collections;
using DefaultNamespace.Enums;
using UnityEngine;

namespace DefaultNamespace
{
    public class WeaponPickup : MonoBehaviour
    {
        [SerializeField] private Renderer renderer;
        [SerializeField] private EWeaponType weaponType;
        [SerializeField] private float delay = 10f;
        private bool isActive = true;

        public EWeaponType WeaponType => weaponType;

        public IEnumerator DelayRoutine()
        {
            isActive = false;
            renderer.enabled = false;
            yield return new WaitForSeconds(delay);
            renderer.enabled = true;
            isActive = true;
        }

        public bool IsActive => isActive;
    }
}